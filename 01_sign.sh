#!/bin/bash

for i in x86_64/*.zst;
do
    echo "Удаление подписи $i.sig"
    rm -f $i.sig
    echo "Добавление подписи $i.sig"
    gpg --detach-sign --local-user BC8B600E8DDA1F4CB77B10D2BA803A261A5EE6B8 --output "$i.sig" "$i"
done

for i in x86_64/*.xz;
do
    echo "Удаление подписи $i.sig"
    rm -f $i.sig
    echo "Добавление подписи $i.sig"
    gpg --detach-sign --local-user BC8B600E8DDA1F4CB77B10D2BA803A261A5EE6B8 --output "$i.sig" "$i"
done
