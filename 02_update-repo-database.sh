#!/bin/bash
#set -e
##################################################################################################################
# Author 	: 	Valeria Fadeeva
# Website : 	https://valeria.fadeeva.me
# Website	:	https://fadeeva.me
# Website	:	https://github.com/Valeria-Fadeeva
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

repo="melawy-archlinux"

rm ./x86_64/$repo.db
rm ./x86_64/$repo.files
rm ./x86_64/$repo.db.sig
rm ./x86_64/$repo.files.sig
rm ./x86_64/$repo.db.tar.gz
rm ./x86_64/$repo.files.tar.gz
rm ./x86_64/$repo.db.tar.gz.sig
rm ./x86_64/$repo.files.tar.gz.sig

repo-add --verify --sign --key BC8B600E8DDA1F4CB77B10D2BA803A261A5EE6B8 --new --remove ./x86_64/$repo.db.tar.gz ./x86_64/*.pkg.tar.xz

mv -f ./x86_64/$repo.db.tar.gz ./x86_64/$repo.db
mv -f ./x86_64/$repo.files.tar.gz ./x86_64/$repo.files
mv -f ./x86_64/$repo.db.tar.gz.sig ./x86_64/$repo.db.sig
mv -f ./x86_64/$repo.files.tar.gz.sig ./x86_64/$repo.files.sig

echo "################################################################"
echo "###################    Update Done        ######################"
echo "################################################################"
